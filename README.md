# mac_bypass
This is a tool to bypass a WiFi access point with login mechanism and the ability of automatic MAC spoofing.

Use this tool at your own risk. The misuse of the code can result in criminal charges brought against the persons in question. I will not be held responsible in the event any criminal charges be brought against any individuals misusing the tool to break the law.
